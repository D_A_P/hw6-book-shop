'use strict';

const cardBooks = document.querySelectorAll('#card-books');
const btnTheme = document.getElementById('btn-theme');

const theme = localStorage.getItem('theme')

cardBooks.forEach((item) => {
    if (theme !== 'light-theme') { 
        item.classList.add('dark-theme') }   
});  

btnTheme.addEventListener('click', function() {
    cardBooks.forEach((item) => {
        
     if(item.classList.contains('light-theme')) {
        item.classList.remove('light-theme');
        item.classList.add('dark-theme');
        localStorage.setItem('theme','dark-theme');
                  
    } else {
        item.classList.remove('dark-theme');
        item.classList.add('light-theme');
        localStorage.setItem('theme','light-theme');
    }     
    });
});

